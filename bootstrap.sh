#!/usr/bin/env bash

if [ ! -f /root/easybuild_installed ]; then
	cd ~
	yum install -y epel-release
	yum install -y python2-pip lua-posix lua lua-filesystem lua-devel tclsh wget expectk
	wget https://sourceforge.net/projects/lmod/files/Lmod-7.8.tar.bz2
	tar -jxf Lmod-7.8.tar.bz2
	mkdir -p /opt/{Lmod,easybuild}
	chown vagrant /opt/easybuild
	cd Lmod-7.8
	./configure --prefix=/opt/Lmod
	make install
	echo "/opt/easybuild/modules/all" >> /opt/Lmod/lmod/lmod/init/.modulespath
	ln -s /opt/Lmod/lmod/lmod/init/profile /etc/profile.d/lmod.sh
	source /opt/Lmod/lmod/lmod/init/bash
	wget -O /tmp/bootstrap_eb.py https://raw.githubusercontent.com/easybuilders/easybuild-framework/develop/easybuild/scripts/bootstrap_eb.py
	chmod u+x /tmp/bootstrap_eb.py
	chown vagrant /tmp/bootstrap_eb.py
	su vagrant -c "/tmp/bootstrap_eb.py /opt/easybuild"
	echo "EASYBUILD_PREFIX=/opt/easybuild" >> /etc/profile.d/easybuild.sh
	echo "EASYBUILD_INSTALLPATH=/opt/easybuild" >> /etc/profile.d/easybuild.sh
	if [ -d /opt/easybuild/software ]; then
		touch /root/easybuild_installed
	fi
fi
